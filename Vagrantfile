# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure('2') do |config|
  config.vm.box = 'hashicorp/bionic64'

  config.vm.define 'app1' do |app_config|
    app_config.vm.hostname = 'app1'
    app_config.vm.network 'forwarded_port', guest: 3000, host: 3000
    app_config.vm.network :private_network, ip: '192.168.50.21'

    app_config.vm.provider 'virtualbox' do |vb|
      vb.memory = 512
      vb.cpus = 1
      vb.name = 'app1_vm'
    end
  end

  config.vm.define 'app2' do |app_config|
    app_config.vm.hostname = 'app2'
    app_config.vm.network 'forwarded_port', guest: 3000, host: 4000
    app_config.vm.network :private_network, ip: '192.168.50.22'

    app_config.vm.provider 'virtualbox' do |vb|
      vb.memory = 512
      vb.cpus = 1
      vb.name = 'app2_vm'
    end
  end

  config.vm.define 'db' do |db_config|
    db_config.vm.hostname = 'db'
    db_config.vm.network :forwarded_port, guest: 22, host: 2000, id: 'ssh'
    db_config.vm.network :private_network, ip: '192.168.50.20'

    db_config.vm.provider 'virtualbox' do |vb|
      vb.memory = 512
      vb.cpus = 1
      vb.name = 'db_vm'
    end
  end

  config.vm.define 'lb' do |lb_config|
    lb_config.vm.hostname = 'lb'
    lb_config.vm.network :forwarded_port, guest: 80, host: 8080
    lb_config.vm.network :private_network, ip: '192.168.50.30'

    lb_config.vm.provider 'virtualbox' do |vb|
      vb.memory = 512
      vb.cpus = 1
      vb.name = 'lb_vm'
    end
  end

  config.vm.define 'runner' do |runner_config|
    runner_config.vm.hostname = 'runner'
    runner_config.vm.network :forwarded_port, guest: 22, host: 2210, id: 'ssh'
    runner_config.vm.network :private_network, ip: '192.168.50.31'

    runner_config.vm.provider 'virtualbox' do |vb|
      vb.memory = 1024
      vb.cpus = 1
      vb.name = 'runner_vm'
    end
  end
end
